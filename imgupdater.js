try{
var Twit = require('twit');
var fs = require('fs');
var path = require('path');
var Canvas = require('canvas');
var Image = Canvas.Image;
var imgurl = "";
var mode = 1;
var mask = true;
var test = true;
const debug = false;
const imgName = "Example.png";



var allowedPixels = [];

var imgData,idata,originalData;


if(mask){
	allowedPixels = require('./mask.json');
}

function endmode(mode){
  console.log("|##| Ending "+mode+"-mode...");
  imgurl = '';
}
var T;

if(!test){
	
	const credentials = require('./credentials.json');
T = new Twit({
  consumer_key:         credentials.consumer.key,
  consumer_secret:      credentials.consumer.secret,
  access_token:         credentials.access_token.key,
  access_token_secret:  credentials.access_token.secret,
  timeout_ms:           60*2000,  // optional HTTP request timeout to apply to all requests.
});
}

//DEBUG:
//imgurl = path.join(__dirname, 'birthday-cake.png');
updateProfileimage();
console.log("DBG1");

function updateProfileimage(){

            //Generate the canvas
            var canvas = new Canvas.createCanvas(400, 400);
            var context = canvas.getContext('2d');

      if(mode == 0){

            var r = Math.floor((Math.random() * 256));
            var g = Math.floor((Math.random() * 256));
            var b = Math.floor((Math.random() * 256));
            var r2 = Math.floor((Math.random() * 256));
            var g2 = Math.floor((Math.random() * 256));
            var b2 = Math.floor((Math.random() * 256));
            var color = "rgb("+r+","+g+","+b+")";
            var color2 = "rgb("+r+","+g2+","+b2+")";


      // draw box

      context.beginPath();
      context.moveTo(0, 0);
      context.lineTo(0, canvas.width/2);
      context.lineTo(canvas.width/2, canvas.width/2);
      context.lineTo(canvas.width/2, 0);
      context.closePath();

      context.lineWidth = 1;
      context.fillStyle = color;
      context.fill();

      //duplicate that but with a different place

      context.beginPath();
      context.moveTo(canvas.width/2, canvas.width/2);
      context.lineTo(canvas.width/2, canvas.width);
      context.lineTo(canvas.width, canvas.width);
      context.lineTo(canvas.width, canvas.width/2);
      context.closePath();

      context.lineWidth = 1;
      context.fillStyle = color;
      context.fill();



      //same with color 2.

      context.beginPath();
      context.moveTo(canvas.width/2, canvas.width/2);
      context.lineTo(0, canvas.width/2);
      context.lineTo(0, canvas.width);
      context.lineTo(canvas.width/2, canvas.width);
      context.closePath();

      context.lineWidth = 1;
      context.fillStyle = color2;
      context.fill();


      //Square 4

      context.beginPath();
      context.moveTo(canvas.width/2, canvas.width/2);
      context.lineTo(canvas.width/2, 0);
      context.lineTo(canvas.width, 0);
      context.lineTo(canvas.width, canvas.width/2);
      context.closePath();

      context.lineWidth = 1;
      context.fillStyle = color2;
      context.fill();
    }else if(mode == 1){


      var cw=canvas.width;
      var ch=canvas.height;



      var img=new Image();
      img.onload=start;
      img.src=path.join(__dirname,'/' + imgName);
      function start(){
        cw=canvas.width=img.width;
        ch=canvas.height=img.height;
        context.drawImage(img,0,0);

        imgData=context.getImageData(0,0,cw,ch);
        idata=imgData.data;
        imgData1=context.getImageData(0,0,cw,ch);
        originalData=imgData1.data;

        HueShift(0,360,-Math.floor((Math.random() * 100))/100);
      }

    }

    if(mode == 0){
      var img = new Image();
      img.onload = function() {
        context.drawImage(img, canvas.width/4, canvas.height/4, canvas.width/2, canvas.height/2);
      }
      img.src = imgurl;
    }
            var out = fs.createWriteStream(__dirname + '/icon.png')
            var stream = canvas.pngStream();
            var dataUrl = canvas.pngStream().pipe(out);
            console.log("Created and wrote icon.");

            if(!test){
                T.post('account/update_profile_image', { image: canvas.toBuffer().toString('base64') }, function (err, data, response) {
                      var message = "Error while updating... Is the mode set?";
                      switch (mode) {
                        case 0:
                          message = "|##| Update Profile Image", err, "1:", color, "2:", color2;
                          break;
                        case 1:
                          message = "|##| Updated Profile Image:", img.src;
                        default:
                          break;
                      }
                      console.log(message);
                      process.exit(0);
                });
            }















            function HueShift(hue1,hue2,shift){
              console.log("Running hue shift.");
              for(var i=0;i<idata.length;i+=4){
                if(!allowedPixels.includes(i))
                  continue;
                red=originalData[i+0];
                green=originalData[i+1];
                blue=originalData[i+2];
                alpha=originalData[i+3];

                // skip transparent/semiTransparent pixels
                if(alpha<230){continue;}

                var hsl=rgbToHsl(red,green,blue);
                var hue=hsl.h*360;

                // change redish pixels to the new color
                //if(hue<0 || hue>360){


                  var newRgb=hslToRgb(hsl.h+shift,hsl.s,hsl.l);
                  idata[i+0]=newRgb.r;
                  idata[i+1]=newRgb.g;
                  idata[i+2]=newRgb.b;
                  idata[i+3]=255;
                //}
                if(test && debug){
                  console.log("Shifting pixel:",i, " R",red," G", green, " B", blue, " A", alpha);
                }
              }
              context.putImageData(imgData,0,0);
                console.log("HueShift done.","shift:", shift,"hue1:", hue1, "hue2:", hue2,"hsl:", hsl, "hue", hue);
            }






            ////////////////////////
            // Helper functions
            //

            function rgbToHsl(r, g, b){
              r /= 255, g /= 255, b /= 255;
              var max = Math.max(r, g, b), min = Math.min(r, g, b);
              var h, s, l = (max + min) / 2;
              if(max == min){
                h = s = 0; // achromatic
              }else{
                var d = max - min;
                s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
                switch(max){
                  case r: h = (g - b) / d + (g < b ? 6 : 0); break;
                  case g: h = (b - r) / d + 2; break;
                  case b: h = (r - g) / d + 4; break;
                }
                h /= 6;
              }
              return({ h:h, s:s, l:l });
            }

            function hslToRgb(h, s, l){
              var r, g, b;
              if(s == 0){
                r = g = b = l; // achromatic
              }else{
                function hue2rgb(p, q, t){
                  if(t < 0) t += 1;
                  if(t > 1) t -= 1;
                  if(t < 1/6) return p + (q - p) * 6 * t;
                  if(t < 1/2) return q;
                  if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
                  return p;
                }
                var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
                var p = 2 * l - q;
                r = hue2rgb(p, q, h + 1/3);
                g = hue2rgb(p, q, h);
                b = hue2rgb(p, q, h - 1/3);
              }
              return({
                r:Math.round(r * 255),
                g:Math.round(g * 255),
                b:Math.round(b * 255),
              });
            }










}


if(!test)
T.get('account/verify_credentials', { skip_status: true })
  .catch(function (err) {
    console.log('caught error', err.stack)
  })
  .then(function (result) {
    // `result` is an Object with keys "data" and "resp".
    // `data` and `resp` are the same objects as the ones passed
    // to the callback.
    // See https://github.com/ttezel/twit#tgetpath-params-callback
    // for details.

    //console.log('data', result.data);
  })

}catch(err){
console.log(err);
}
