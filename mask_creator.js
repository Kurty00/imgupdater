var Canvas = require('canvas');
var fs = require('fs');
var path = require('path');
var Image = Canvas.Image;
var imgData,idata,originalData;

//Generate the canvas
var canvas = new Canvas.createCanvas(512, 512);
var context = canvas.getContext('2d');

var arr = [];

var img=new Image();
img.onload=start;
img.src=path.join(__dirname,'/mask.png');
      function start(){
        cw=canvas.width=img.width;
        ch=canvas.height=img.height;
        context.drawImage(img,0,0);

        imgData=context.getImageData(0,0,cw,ch);
        idata=imgData.data;
        imgData1=context.getImageData(0,0,cw,ch);
        originalData=imgData1.data;

        for(var i=0;i<idata.length;i+=4){
                red=originalData[i+0];
                green=originalData[i+1];
                blue=originalData[i+2];
                alpha=originalData[i+3];

				if(red === 0 && green === 0 && blue === 0 && alpha === 255) {
					arr.push(i);
				}
		}
}


fs.writeFile('mask.json',
              JSON.stringify(arr),
              'utf8',
              err => {
                if(err) {
                  console.log(err)
                }
              });
